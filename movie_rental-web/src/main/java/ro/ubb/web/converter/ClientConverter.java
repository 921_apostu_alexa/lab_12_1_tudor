package ro.ubb.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.core.model.Client;
import ro.ubb.web.dto.ClientDto;

@Component
public class ClientConverter extends BaseConverter<Client, ClientDto> {
    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client client = Client.builder()
                .name(dto.getName())
                .email(dto.getEmail())
                .build();
        client.setId(dto.getId());
        return client;
    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto dto = ClientDto.builder()
                .name(client.getName())
                .email(client.getEmail())
                .build();
        dto.setId(client.getId());
        return dto;
    }
}
