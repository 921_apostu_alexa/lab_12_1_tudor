package ro.ubb.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.core.model.Rental;
import ro.ubb.web.dto.RentalDto;

@Component
public class RentalConverter extends BaseConverter<Rental, RentalDto> {

    @Override
    public Rental convertDtoToModel(RentalDto dto) {
        Rental rental = Rental.builder()
                .clientId(dto.getClientId())
                .movieId(dto.getMovieId())
                .rentDate(dto.getRentDate())
                .build();
        rental.setId(dto.getId());
        return rental;
    }

    @Override
    public RentalDto convertModelToDto(Rental rental) {
        RentalDto dto = RentalDto.builder()
                .clientId(rental.getClientId())
                .movieId(rental.getMovieId())
                .rentDate(rental.getRentDate())
                .build();
        dto.setId(rental.getId());
        return dto;
    }
}
