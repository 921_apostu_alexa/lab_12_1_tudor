package ro.ubb.web.converter;

import ro.ubb.core.model.BaseClass;
import ro.ubb.web.dto.BaseDto;

public interface Converter<Model extends BaseClass<Long>, Dto extends BaseDto> {

    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);

}

