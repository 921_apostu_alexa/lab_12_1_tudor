import { Component, OnInit } from '@angular/core';
import {Client} from "../../../model/client.model";
import {ClientService} from "../../../service/client.service";
import {cold} from "jasmine-marbles";

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {

  submitted = false;
  client = new Client();

  constructor( private clientService: ClientService) { }

  ngOnInit(): void {
  }

  addClient() {
    console.log("added client: ", this.client);
    this.clientService.saveClient(this.client).subscribe();
  }

  updateClient(){
    console.log("update client: ", this.client);
      this.clientService.updateClient(this.client).subscribe();
  }

  deleteClient(){
    console.log("delete client with ID : ", this.client.id);
    this.clientService.removeClient(this.client.id);
  }

  onSubmit() {
    this.submitted = true;
  }
}
