import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Rental} from "../model/rental.model";

@Injectable({
  providedIn: "root"
})

export class RentalService {
  private rentalsUrl = "http://localhost:8082/api/rentals";

  constructor(private httpClient:HttpClient) {
  }

  getAllRentals(): Observable<Rental[]>{
    return this.httpClient.get<Array<Rental>>(this.rentalsUrl);
  }

  getRental(id: number): Observable<Rental>{
    const rentalId = `${this.rentalsUrl}/{id}`;
    return this.httpClient.get<Rental>(rentalId);
  }

  saveRental(rental: Rental): Observable<Rental>{
    return this.httpClient.post<Rental>(this.rentalsUrl, rental);
  }

  removeRental(id: number): Observable<any>{
    const rentalUrl = `${this.rentalsUrl}/{id}`;
    return this.httpClient.delete(rentalUrl);
  }

}
