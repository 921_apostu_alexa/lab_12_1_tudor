import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Movie} from "../model/movie.model";

@Injectable({
  providedIn: 'root'
})

export class MovieService {
  private movieUrl = "http://localhost:8082/api/movies";

  constructor(private httpClient: HttpClient) {
  }

  getAllMovies(): Observable<Movie[]>{
    return this.httpClient.get<Array<Movie>>(this.movieUrl);
  }

  getMovie(id: number): Observable<Movie>{
    const movieUrl = `${this.movieUrl}/{id}`;
    return this.httpClient.get<Movie>(movieUrl);
  }

  saveMovie(movie: Movie): Observable<Movie>{
    return this.httpClient.post<Movie>(this.movieUrl, movie);
  }
  removeMovie(id: number): Observable<any>{
    const movieUrl = `${this.movieUrl}/{id}`;
    return this.httpClient.delete(movieUrl);
  }
}


