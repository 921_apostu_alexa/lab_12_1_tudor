import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientComponent} from "./components/clients/client.component";
import {MoviesComponent} from "./components/movies/movies.component";
import {RentalsComponent} from "./components/rentals/rentals.component";


const routes: Routes = [
  { path: 'clients', component:ClientComponent },
  {path: 'movies', component: MoviesComponent},
  {path: 'rentals', component: RentalsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
