import {Component, OnInit} from '@angular/core';
import {Rental} from "../../model/rental.model";
import {RentalService} from "../../service/rental.service";

@Component({
  selector: 'app-rentals',
  templateUrl: './rentals.component.html',
  styleUrls: ['./rentals.component.css']
})
export class RentalsComponent implements OnInit {
  rentalsList: Rental[];
  currentRental: Rental;
  errorMessage: string;

  constructor(private rentalService: RentalService) {
  }

  ngOnInit(): void {
    this.getAllRentals();
  }

  getAllRentals() {
    this.rentalService.getAllRentals().subscribe(rentals => {
        console.log(rentals);
        this.rentalsList = rentals;
      },
      error => {
        this.errorMessage = <any>error
      }
    );
  }

  onClick(rental: Rental){
    this.currentRental = rental;
  }
}
