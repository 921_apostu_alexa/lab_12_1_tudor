export class Rental {
  clientId: number;
  movieId: number;
  rentDate: Date;

  constructor(_clientId: number, _movieId: number, _rentDate: Date) {
    this.clientId = _clientId;
    this.movieId = _movieId;
    this.rentDate = _rentDate;
  }
}
