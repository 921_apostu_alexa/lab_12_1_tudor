import {Component, OnInit} from '@angular/core';
import {Movie} from "../../model/movie.model";
import {MovieService} from "../../service/movie.service";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  moviesList: Movie[];
  currentMovie: Movie;
  errorMessage: string;

  constructor(private movieService: MovieService) {

  }

  ngOnInit(): void {
    this.getAllMovies();
  }

  getAllMovies() {
    this.movieService.getAllMovies().subscribe(movies => {
        console.log(movies);
        this.moviesList = movies;
      },
      error => {
        this.errorMessage = <any>error
      }
    );
  }
  onClick(movie: Movie){
    this.currentMovie = movie;
  }
}
