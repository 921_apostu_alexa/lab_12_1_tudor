import { Component, OnInit } from '@angular/core';
import {Client} from "../../model/client.model";
import {ClientService} from "../../service/client.service";

@Component({
  selector: 'app-clients',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  clientsList: Client[];
  currentClient: Client;
  errorMessage: string;
  constructor(private clientService: ClientService) {

  }
  //get() returns Observables, and they can be accessed with the Subscribe method
  ngOnInit(): void {
    this.getAllClients();
  }
  getAllClients(){
    this.clientService.getAllClients().subscribe(clients =>{
        console.log(clients);
        this.clientsList = clients;
      },
      error => this.errorMessage = <any>error
    );
  }
  onClick(client: Client){
    this.currentClient = client;
  }

}
