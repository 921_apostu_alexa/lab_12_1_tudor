export class Movie {

  constructor(
    public id?: number,
    public title?: string,
    public minutes?: number,
    public year?: number,
  ) { }
}
