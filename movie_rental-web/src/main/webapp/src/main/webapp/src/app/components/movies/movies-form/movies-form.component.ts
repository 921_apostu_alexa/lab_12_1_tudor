import { Component, OnInit } from '@angular/core';
import {Movie} from "../../../model/movie.model";
import {MovieService} from "../../../service/movie.service";

@Component({
  selector: 'app-movies-form',
  templateUrl: './movies-form.component.html',
  styleUrls: ['./movies-form.component.css']
})
export class MoviesFormComponent implements OnInit {
  submitted = false;
  movie = new Movie();

  constructor( private movieService: MovieService) { }

  ngOnInit(): void {
  }

  addMovie() {
    console.log("added movie: ", this.movie);
    this.movieService.saveMovie(this.movie).subscribe();
  }

  deleteMovie(){
    console.log("delete movie with ID : ", this.movie.id);
    this.movieService.removeMovie(this.movie.id);
  }

  onSubmit() {
    this.submitted = true;
  }

}
