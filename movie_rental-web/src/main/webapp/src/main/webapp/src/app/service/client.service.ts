import { Injectable } from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Client} from "../model/client.model";

@Injectable({
  providedIn: 'root'
})

export class ClientService {
  private clientsUrl = "http://localhost:8082/api/clients";

  constructor(private httpClient: HttpClient) {
  }
  getAllClients(): Observable<Client[]>{
    return this.httpClient.get<Array<Client>>(this.clientsUrl);
  }

  getClient(id: number): Observable<Client>{
    const clientUrl = `${this.clientsUrl}/{id}`;
    return this.httpClient.get<Client>(clientUrl);
  }

  saveClient(client: Client): Observable<Client>{
    return this.httpClient.post<Client>(this.clientsUrl, client);
  }

  updateClient(client: Client): Observable<Client>{
    return this.httpClient.put<Client>(this.clientsUrl, client);
  }

  removeClient(id: number): Observable<any>{
    const clientUrl = `${this.clientsUrl}/{id}`;
    return this.httpClient.delete(clientUrl);
  }
}
