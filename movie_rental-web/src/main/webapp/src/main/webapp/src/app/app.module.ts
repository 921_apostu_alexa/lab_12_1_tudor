import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from "@angular/core";
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import { ClientComponent } from './components/clients/client.component';
import {ClientService} from "./service/client.service";
import { MoviesComponent } from './components/movies/movies.component';
import { RentalsComponent } from './components/rentals/rentals.component';
import {FormsModule} from "@angular/forms";
import { ClientFormComponent } from './components/clients/client-form/client-form.component';
import { MoviesFormComponent } from './components/movies/movies-form/movies-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientComponent,
    MoviesComponent,
    RentalsComponent,
    ClientFormComponent,
    MoviesFormComponent,
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],

  providers: [ClientService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
