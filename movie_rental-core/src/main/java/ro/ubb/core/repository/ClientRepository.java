package ro.ubb.core.repository;

import ro.ubb.core.model.Client;

public interface ClientRepository extends IRepository<Client, Long> {
}
