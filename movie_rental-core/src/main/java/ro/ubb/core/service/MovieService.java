package ro.ubb.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.core.model.Movie;
import ro.ubb.core.repository.MovieRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService {
    public static final Logger log = LoggerFactory.getLogger(MovieService.class);

    @Autowired
    private MovieRepository repository;

    public Movie getMovie(Long ID) {
        log.trace("getMovie -- method entered");
        Movie movie = this.repository.findById(ID).orElse(new Movie());
        log.trace("getMovie: movie={}", movie);
        return movie;
    }

    public List<Movie> getAllMovies() {
        log.trace("getAllMovies --- method entered");
        List<Movie> result = repository.findAll();
        log.trace("getAllMovies: result={}", result);
        return result;
    }

    public Movie saveMovie(Movie movie) {
        log.trace("saveMovie -- method entered");
        Movie savedMovie = repository.save(movie);
        log.trace("saveMovie -- savedMovie={}", savedMovie);
        return savedMovie;
    }

    @Transactional
    public Movie updateMovie(Long id, Movie movie) {
        log.trace("updateMovie -- method entered");

        Movie updatedMovie = repository.findById(id).orElse(movie);
        updatedMovie.setTitle(movie.getTitle());
        updatedMovie.setMinutes(movie.getMinutes());
        updatedMovie.setYear(movie.getYear());

        log.trace("updateMovie -- updatedMovie={}", updatedMovie);
        return updatedMovie;
    }

    public void deleteMovie(Long id) {
        log.trace("deleteMovie -- method entered");
        repository.deleteById(id);
        log.trace("deleteMovie -- removed movie with id={}", id);
    }

    public List<Movie> filterMoviesByYear(int year) {
        return this.repository.findAll().stream()
                .filter(movie -> movie.getYear() == year)
                .collect(Collectors.toList());
    }
}