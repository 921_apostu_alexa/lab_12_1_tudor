package ro.ubb.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.core.model.Client;
import ro.ubb.core.model.Movie;
import ro.ubb.core.model.Rental;
import ro.ubb.core.model.exceptions.BaseException;
import ro.ubb.core.model.exceptions.ValidatorException;
import ro.ubb.core.repository.ClientRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ClientService {
    public static final Logger log = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private ClientRepository repo;

    public Client getClient(Long ID) {
        log.trace("getClient -- method entered");
        Client client = this.repo.findById(ID).orElse(new Client());
        log.trace("getClient: client={}", client);
        return client;
    }

    public List<Client> getAllClients() {
        log.trace("getAllClients -- method entered");
        List<Client> result = repo.findAll();
        log.trace("getAllClients: result={}", result);
        return result;
    }

    public Client saveClient(Client client) {
        log.trace("saveClient -- method entered");
        Client savedClient = repo.save(client);
        log.trace("saveClient -- savedClient={}", savedClient);
        return savedClient;
    }

    @Transactional
    public Client updateClient(Long id, Client client) {
        log.trace("updateClient -- method entered");

        Client updatedClient = repo.findById(id).orElse(client);
        updatedClient.setName(client.getName());
        updatedClient.setEmail(client.getEmail());

        log.trace("updateClient -- updatedClient={}", updatedClient);
        return updatedClient;
    }

    public void deleteClient(Long id) {
        log.trace("deleteClient -- method entered");
        repo.deleteById(id);
        log.trace("deleteClient -- removed client with id={}", id);
    }

    public List<Client> filterClientsByName(String name) {
        return this.repo.findAll().stream()
                .filter(client -> client.getName().contains(name))
                .collect(Collectors.toList());
    }
}
