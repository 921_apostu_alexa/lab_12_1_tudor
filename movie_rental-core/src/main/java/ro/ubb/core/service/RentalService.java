package ro.ubb.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.core.model.Rental;
import ro.ubb.core.repository.RentalRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RentalService {
    public static final Logger log = LoggerFactory.getLogger(RentalService.class);

    @Autowired
    private RentalRepository repository;

    public Rental getRental(Long ID) {
        log.trace("getRental -- method entered");
        Rental rental = this.repository.findById(ID).orElse(new Rental());
        log.trace("getRental: rental={}", rental);
        return rental;
    }

    public List<Rental> getAllRentals() {
        log.trace("getAllRentals --- method entered");
        List<Rental> result = repository.findAll();
        log.trace("getAllRentals: result={}", result);
        return result;
    }

    public Rental rent(Long clientId, Long movieId) {
        log.trace("rent -- method entered: clientId={}, movieId={}", clientId, movieId);

        Rental rental = new Rental(clientId, movieId, new Date());
        Rental savedRental = this.repository.save(rental);
        log.trace("rent -- savedRental={}", savedRental);

        return rental;
    }

    @Transactional
    public Rental updateRental(Long id, Rental rental) {
        log.trace("updateRental -- method entered");

        Rental updatedRental = repository.findById(id).orElse(rental);
        updatedRental.setClientId(rental.getClientId());
        updatedRental.setMovieId(rental.getMovieId());
        updatedRental.setRentDate(rental.getRentDate());

        log.trace("updateRental -- updatedRental={}", updatedRental);
        return updatedRental;
    }

    public void deleteRental(Long id) {
        log.trace("deleteRental -- method entered");
        repository.deleteById(id);
        log.trace("deleteRental -- removed rental with id={}", id);
    }

    public void removeRentalsWithClientID(Long clientId) {
        this.repository.findAll().stream()
                .filter(rental -> rental.getClientId().equals(clientId))
                .forEach(rental -> this.repository.delete(rental));
    }

    public void removeRentalsWithMovieID(Long movieId) {
        this.repository.findAll().stream()
                .filter(rental -> rental.getMovieId().equals(movieId))
                .forEach(rental -> this.repository.delete(rental));
    }

    public List<Rental> filterRentalsByClient(Long clientID) {
        return this.repository.findAll().stream()
                .filter(rental -> rental.getClientId().equals(clientID))
                .collect(Collectors.toList());
    }

    public List<Rental> filterRentalsByStartingDate(Date date) {
        return this.repository.findAll().stream()
                .filter(rental -> rental.getRentDate().after(date))
                .collect(Collectors.toList());
    }

    public Long getMostRentedMovieId() {
        return this.repository.findAll().stream()
                .collect(Collectors.groupingBy(Rental::getMovieId, Collectors.counting()))
                .entrySet()
                .stream()
                .max((r1, r2) -> r1.getValue() > r2.getValue() ? 1 : 0)
                .get()
                .getKey();
    }
}
